from time import time
from lib.secop_data.accessible import Accessible
from lib.errors import secop_errors


# This class currently just fakes an idle status (status 100)
# FIXME: Implement proper status
class StatusAccessible(Accessible):
    def update_from_source(self, data):
        pass


    def has_live_data(self):
        return True


    def passively_updated(self):
        return False


    def get(self):
        return [ [100, 'Idle'], { 't': time() } ]


    def set(self):
        pass


    def confirmed_set(self):
        return True


    def update_once_on_activation(self):
        return False
