This library extends the secop_structure library to add data handling capabilities. That means that once you've parsed an Octopy structure report the resulting Node object not only gives access to the structure itself but also reading and writing data to/from the data source/hardware. The actual data transfer is being done via three callbacks that needs to be setup once the structure has been parsed. There are helper methods for this, mainly 'value_topics', 'get_topics', and 'set_topics'. These methods will return a dict with all topics of the corresponding type that are present in any accessible together with a reference to the accessibles themselves. The keys are the topics and the values are lists of the accessibles that use those topics. The use of lists means there is support for multiple accessibles that uses the same topic. Below is a small example code showing how setting up the callbacks can be done (sans error checking):

```
# Parse report and build node structure. Validation is done automatically
report = json.loads(payload)
node = SecNode(report)

# Set callback for value updates from the source
value_topics = node.value_topics()
for value_topic, accessibles in value_topics.items():
    client.subscribe(value_topic)

# Set callback for writing values to the source
set_topics = node.set_topics()
for set_topic, accessibles in set_topics.items():
    for accessible in accessibles:
        accessible.set_callback = on_accessible_set

# Set callback for polled reading from source and also request a new value read
get_topics = node.get_topics()
for get_topic, accessibles in get_topics.items():
    for accessible in accessibles:
        accessible.get_callback = on_accessible_get
        accessible.poll()  # Request a new value immediately - requires the other callbacks to be setup correctly first
```


The method on the Accessible objects for updating a value is called "update_from_source" and takes a single argument which is the value in SECoP-format, i.e. a list with one or two items with the actual value at index 0 and an optional dict with quantifiers at index 1.

The callback for setting a value needs to take two arguments. The first is the Accessible object that called it and the second is the value to be set in SECoP-format (see above).

The callback for requesting a new value should take a single argument which is the Accessible object doing the request.

All further information such as what topic to use etc can be easily extracted from the Accessible-object. Below is a simple example code to show how it can be done:

```
# Main MQTT message callback
def on_mqtt_message(client, userdata, message):
    global node  # Assumed to be a global variable referencing the main SecNode object
    value_topics = node.value_topics()
    if message.topic in value_topics:
        value = json.loads(message.payload)
        for accessible in value_topics[message.topic]:
            accessible.update_from_source(value)

# Accessible write callback
def on_accessible_set(accessible, value):
    global mqtt_client  # Assumed to be a global variable referencing the MQTT client used for communication
    topic = accessible.set_topic
    payload = json.dumps(value)
    mqtt_client.publish(topic, payload, qos = 1)

# Accessible read request callback
def on_accessible_get(accessible):
    global mqtt_client  # Assumed to be a global variable referencing the MQTT client used for communication
    topic = accessible.get_topic
    mqtt_client.publish(topic, None, qos = 1)
```


Dependencies:
* lib/secop_structure
* lib/errors
* lib/secop_datainfo
* json
