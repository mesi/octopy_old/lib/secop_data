import logging
from lib.secop_structure.accessible import Accessible as BaseAccessible
from lib.secop_datainfo import secop_datainfo
from lib.errors import secop_errors
from time import time

class Accessible(BaseAccessible):
    def __init__(self, conf):
        super().__init__(conf)
        self._set_callback = None  # Callback for updating source/hardware via set_topic
        self._get_callback = None  # Callback for requesting a new value from source/hardware via get_topic
        self._on_change_callback = None
        self._set_value = [ None, {} ]  # The last value that the accessible was set from user/controller
        self._source_value = [ None, {} ]  # The last value received from source/hardware
        self._last_update_from_source = None  # Timestamp for when the accessible was last update from source/hardware
        self._last_set = None  # Timestamp for when the accessible was last set by user/controller
        self._update_once_on_activation_flag = True


    @property
    def set_callback(self):
        return self._set_callback


    @set_callback.setter
    def set_callback(self, func):
        self._set_callback = func


    @property
    def get_callback(self):
        return self._get_callback


    @get_callback.setter
    def get_callback(self, func):
        self._get_callback = func


    @property
    def on_change_callback(self):
        return self._on_change_callback


    @on_change_callback.setter
    def on_change_callback(self, func):
        self._on_change_callback = func


    @property
    def value(self):
        return self._source_value


    @value.setter
    def value(self, val):
        self.set(val)


    def update_from_source(self, data):
        """Updates the Accessible's value with data received from the hardware/source"""
        if not isinstance(data, list):
            raise secop_errors.HardwareError(f"Malformed report received from soruce for '{self.full_name}'")
        value = data[0]
        qualifiers = {}
        #if 'readable' not in self.module().interface_classes:
        #    if abs(self.module().deviation()) >= 0.1:
        #        print('not idle, deviation: {}'.format(self.module().deviation()))
        #        self.module().accessibles.get('status')._data = [[300, 'BUSY'], {'t': time.time()}]
        #    else:
        #        self.module().accessibles.get('status')._data = [[100, 'IDLE'], {'t': time.time()}]
        if len(data) > 1:
            # Option is present, make sure it's valid
            if not isinstance(data[1], dict):
                raise secop_errors.HardwareError(f"Malformed report received from soruce for '{self.full_name}'")
            qualifiers = data[1]
        if isinstance(self.datainfo.type, secop_datainfo.SecCommand):
            # For commands we need to validate against the return value type
            value = self.datainfo.result.validate_value(value)
        else:
            value = self.datainfo.validate_value(value)
        self._source_value[0] = value
        self._source_value[1] = qualifiers
        self.last_update_from_source = time()
        # Report change of value to parent
        self._parent.on_accessible_change(self)
        if self._on_change_callback:
            self._on_change_callback(self, self._source_value)
        return data


    def has_live_data(self):
        return self._source_value[0] != None


    def passively_updated(self):
        """This method returns true if the accessible is assumed to be updated
        using polling rather than automatically getting its values pushed from
        the source - i.e. it has a 'get_topic' defined."""
        return (hasattr(self, 'get_topic'))


    def poll(self):
        """Tells the accessible to request a new value from the source."""
        self.get()


    def get(self):
        """Returns the current value for the accessible.
        If the accessible has a get_topic defined a request for a value update will be
        made to this topic first. This is slightly broken at the moment since there is
        no synchronization being done so it may return an old value."""
        if hasattr(self, 'get_topic') and self.get_topic:
            if not callable(self._get_callback):
                raise secop_error.HardwareError(f"Can't get new value for accessible {self.name}: no get callback function")
            # FIXME: Add synchronization to make sure a new value is received before returning
            self._get_callback(self)
        return self._source_value


    def set(self, data):
        """Sets the Accessible's value and sends it to the hardware/source using a callback"""
        """value: a SECoP compliant value, i.e. a list with the value at index 0 and a dict with qualifiers at index 1"""
        if self.readonly:
            raise secop_errors.ReadOnlyError(f"Can't set accessible {self.name}: readonly")
        if not hasattr(self, 'set_topic') or not self.set_topic:
            #if self.name == 'stop':  # TODO: list
            #    return
            raise secop_error.HardwareError(f"Can't set accessible {self.name}: no set_topic")
        if not callable(self._set_callback):
            raise secop_error.HardwareError(f"Can't set accessible {self.name}: no set callback function")
        value = data[0]
        qualifiers = {}
        if len(data) > 1:
            # Option is present, make sure it's valid
            if not isinstance(data[1], dict):
                raise secop_errors.HardwareError(f"Malformed report received from soruce for '{self.full_name}': qualifier must an object, got {data[1].__class__.__name__}")
            qualifiers = data[1]

        if isinstance(self.datainfo.type, secop_datainfo.SecCommand):
            # For commands the value is used as argument and needs to be validated against the argument's type
            self._set_value[0] = self.datainfo.argument.validate_value(value[0])
        else:
            self._set_value[0] = self.datainfo.validate_value(value)
        self._set_value[1] = qualifiers
        self._set_callback(self, self._set_value)
        self._last_set = time()
        return self._set_value


    def confirmed_set(self):
        return ( (self._source_value[0] == self._set_value[0]) and\
                self._last_update_from_source and self._last_set and\
                (self._last_update_from_source > self._last_set) )


    def update_once_on_activation(self):
        ret = self._update_once_on_activation_flag
        self._update_once_on_activation_flag = False
        return ret
