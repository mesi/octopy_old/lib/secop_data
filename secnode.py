import logging
from lib.secop_structure.secnode import SecNode as BaseSecNode
from lib.secop_data.module import Module
from lib.errors import secop_errors

class SecNode(BaseSecNode):
    def __init__(self, conf):
        super().__init__(conf)
        self._active = False


    @property
    def accessible_change_callback(self):
        return self._on_accessible_change_cb


    @accessible_change_callback.setter
    def accessible_change_callback(self, func):
        self._on_accessible_change_cb = func


    def on_accessible_change(self, accessible):
        if self._on_accessible_change_cb:
            self._on_accessible_change_cb(accessible)


    def activate(self):
        for mod_id, mod in self.modules.items():
            mod.activate()


    def deactivate(self):
        for mod_id, mod in self.modules.items():
            mod.deactivate()


    def deserialize_modules(self, modules_conf):
        # Create/update modules
        for mod_id, mod_conf in modules_conf.items():
            mod_conf['parent'] = self
            mod_conf['id'] = mod_id
            if mod_id in self.modules:
                self.modules[mod_id].deserialize(mod_conf)
            else:
                self.modules[mod_id] = Module(mod_conf)
        # Remove modules no longer present in conf
        for mod_id in self.modules.copy():
            if not mod_id in modules_conf:
                del self.modules[mod_id]


    def all_accessibles_active(self):
        """This method returns true if all accessibles that have a value_topic defined
        has received data at least once. This is mainly used by ECS gateways that require
        current data to be present before they can start.
        Note that passively updated accessibles, i.e. accessibles that has a get_topic
        defined and requires polling to get new values, will cause this method to always
        return false until they have been polled, or in some other way been updated
        with current values."""
        accessibles = self.get_accessibles()
        for accessible in accessibles:
            if hasattr(accessible, 'value_topic') and not accessible.has_live_data():
                # An accessible without live data discovered so the node is not fully alive yet.
                return False
        return True
