from lib.errors import secop_errors
from lib.secop_data.accessible import Accessible
from lib.secop_data.status_accessible import StatusAccessible
from lib.secop_structure.module import Module as BaseModule

class Module(BaseModule):
    def __init__(self, conf):
        super().__init__(conf)
        self._active = False


    def activate(self):
        self._active = True


    def deactivate(self):
        self._active = False


    # Complete copy of the method in the Base Module class.
    # A solution should be found that doesn't require code duplication.
    def deserialize_accessibles(self, accessibles_conf):
        # Create/update accessibles
        for acc_id, acc_conf in accessibles_conf.items():
            acc_conf['parent'] = self
            acc_conf['id'] = acc_id
            if acc_id in self.accessibles:
                self.accessibles[acc_id].deserialize(acc_conf)
            else:
                if acc_id == 'status':
                    self.accessibles[acc_id] = StatusAccessible(acc_conf)
                else:
                    self.accessibles[acc_id] = Accessible(acc_conf)
        # Remove accessibles no longer present in conf
        for acc_id in self.accessibles.copy():
            if not acc_id in accessibles_conf:
                del self.accessibles[acc_id]
        # If no status accessible exists we create a default one
        if not 'status' in self.accessibles:
            value_topic = '/status'
            if 'value' in self.accessibles:
                value_topic = self.accessibles['value'].value_topic + value_topic
            elif len(self.accessibles) > 0 and hasattr(list(self.accessibles.values())[0], 'value_topic'):
                value_topic = list(self.accessibles.values())[0].value_topic + value_topic
            else:
                value_topic = self.id + value_topic
            conf = {
                'id': 'status',
                'description': 'Status',
                'parent': self,
                'readonly': True,
                'octopy': {
                    'links': {
                        'value topic': value_topic
                    }
                }
            }
            self.accessibles['status'] = StatusAccessible(conf)



    def on_accessible_change(self, accessible):
        """Callback used by child accessible nodes to indicate that they've
        received a new value from the source"""
        if self._active:
            self._parent.on_accessible_change(accessible)

    """
    def deviation(self):
        if not hasattr(self, 'target'):
            raise secop_errors.NoSuchParameterError(f"Can't calculate module deviation: Module {self.full_name} has no 'target' accessible")
        if not hasattr(self, 'value'):
            raise secop_errors.NoSuchParameterError(f"Can't calculate module deviation: Module {self.full_name} has no 'value' accessible")
        target = self.accessibles.get('target').get()[0]
        value = self.accessibles.get('value').get()[0]
        if not value:
            raise secop_errors.NoSuchParameterError(f"Can't calculate module deviation: accessible 'value' did not return a valid value")
        if not target:
            raise secop_errors.NoSuchParameterError(f"Can't calculate module deviation: accessible 'target' did not return a valid value")
        return 100*(target-val)/target
    """
